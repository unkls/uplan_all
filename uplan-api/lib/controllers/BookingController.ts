import * as mongoose from 'mongoose';
import * as nodemailer from 'nodemailer';
import * as moment from 'moment';
import { Request, Response } from 'express';
import { BookingSchema } from '../models/BookingModel';
import { UserSchema } from '../models/UserModel';
import { SupplySchema } from '../models/SupplyModel';
import { SupplyTypeSchema } from '../models/SupplyTypeModel';



import axios from 'axios';

const Books = mongoose.model('Books', BookingSchema);
const Users = mongoose.model('User', UserSchema);
const Supplies = mongoose.model('Supply', SupplySchema);
const SupplyType = mongoose.model('SupplyType', SupplyTypeSchema);

const MailTransporter =  nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "488ee642549c27",
      pass: "8eb698288e1981"
    }
});

export class UplanBookingController{
    public needBook (req: Request, res: Response) {   
        let supplies = Supplies.findOne({"_id":req.body.Supply}).populate('SupplyType').exec();
        let user = Users.findOne({"_id":req.body.User}).exec();
        Promise.all([supplies, user]).then(([supply, user])=>{
            let newBook = new Books(req.body);
            newBook.save((err, newBook) => {
                if(err){
                    res.send(err);
                }
                var htmlHead= "<h1>Nouvelle demande de réservation</h1><br><ul><li>Début : "
                + moment(newBook.start, "X").format("DD/MM/YYYY HH:mm") +"</li><li>Fin : "
                + moment(newBook.end, "X").format("DD/MM/YYYY HH:mm") +"</li><li>Sujet: "
                + req.body.MessageContent +"</li><ul>"

                var mail = {
                    from: user.email,
                    to: supply.SupplyType.emailRefferer,
                    subject: "Demande de réservation",
                    html: htmlHead+'</br></br><a href="http://localhost:3000/acceptbook/'+newBook._id+'/accept">Accepter</a><a href="http://localhost:3000/acceptbook/'+newBook._id+'/refuse">Refuser</a>'
                }
                MailTransporter.sendMail(mail, (error, response) => {
                    if(error){
                        console.log("Erreur lors de l'envoie du mail!");
                        console.log(error);
                    }else{
                        console.log("Mail envoyé avec succès!")
                    }
                    MailTransporter.close();
                    let data = {'State':'success', 'Message':'Book type add successfully', 'Book': newBook}
                    res.json(data)  
                });  
            });
        });
    }  

    public userBook (req: Request, res: Response) {    
        Books.find({User:req.params.userId, needAccept:false}, (err, books) => {
            if(err)
                res.send(err);
            res.json(books);
        }).populate('User').populate({
            path    : 'Supply',
            populate : {path : 'SupplyType'}
       });
    } 

    public acceptBook (req: Request, res: Response) {                
        let bookId = req.params.bookId
        let action = req.params.action

        if(action === "refuse"){
            Books.findOne({"_id":bookId}, (err, book) => {
                if(err)
                    res.send(err);
                
                var mail = {
                    from: book.Supply.SupplyType.emailRefferer,
                    to: book.User.email,
                    subject: "Demande de réservation refusée",
                    html: "Votre de demande de réservation n'a pas été acceptée car le motif n'est pas valable.<br> Cordialement"          
                }
                MailTransporter.sendMail(mail, (error, response) => {
                    if(error){
                        console.log("Erreur lors de l'envoie du mail!");
                        console.log(error);
                    }else{
                        console.log("Mail envoyé avec succès!")
                    }
                    MailTransporter.close();
                    book.remove()
                });
            }).populate('User').populate({
                path    : 'Supply',
                populate : {path : 'SupplyType'}
           });
        } else {
            Books.update({ "_id":bookId }, {
                needAccept: false, 
            }, (err) => {
                if(err)
                    res.send(err);
            });
        }

        let data = {'State':'success', 'Message':'Book type update successfully'}
        res.json(data)  
    }    
}