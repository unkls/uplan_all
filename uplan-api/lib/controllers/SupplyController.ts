
import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import { SupplySchema } from '../models/SupplyModel';
import { SupplyTypeSchema } from '../models/SupplyTypeModel';
import { BookingSchema } from '../models/BookingModel';

import axios from 'axios';
import * as moment from 'moment/moment';

const Supplies = mongoose.model('Supplies', SupplySchema);
const SupplyType = mongoose.model('SupplyType', SupplyTypeSchema);
const Books = mongoose.model('Books', BookingSchema);

export class UplanSupplyController{
    public addSupply (req: Request, res: Response) {                
        let newSupply = new Supplies(req.body);
        newSupply.save((err, newSupply) => {
            if(err){
                res.send(err);
            }
            let data = {'State':'success', 'Message':'Supply add successfully', 'Supply': newSupply}
            res.json(data)    
        });
    }

    public supply (req: Request, res: Response) {                
        Supplies.findOne({"_id":req.params.supplyId}, (err, supply) => {
            if(err)
                res.send(err);
            res.json(supply);
        }).populate('SupplyType');
    }

    public allSupplies (req: Request, res: Response) {      
        let supplies = Supplies.find({}).populate('SupplyType').exec();
        let books =  Books.find({needAccept:false}).exec();          
        Promise.all([supplies, books]).then(([supply, book])=>{
            supply.forEach(supplyelement => {
                book.forEach(bookelement => {
                    if(bookelement.Supply.toString() === supplyelement._id.toString())
                        supply.splice(supply.indexOf(supplyelement), 1);
                });
            });


            res.json(supply)
        });
    }
}