import * as mongoose from 'mongoose';
import { UserSchema } from '../models/UserModel';
import { Request, Response } from 'express';

import axios from 'axios';
import * as moment from 'moment/moment';

const Users = mongoose.model('Users', UserSchema);


export class UplanUserController{

    public addUser (req: Request, res: Response) {                
        let newUser = new Users(req.body);
        newUser.save((err, user) => {
            if(err){
                res.send(err);
            }
            let data = {'State':'success', 'Message':'User add successfully', 'User': user}
            res.json(data)    
        });
    }

    public connectUser (req: Request, res: Response) {               
        let identifier = req.body;
        let UserPromise = Users.findOne({email:identifier.email, password:identifier.password}).exec();
        UserPromise.then(user => {
            let state, message:String;
            let userlogged:Object;
            if (user) { 
                state = 'success';
                message = 'User exists';
                userlogged = user;
            } else {
                state = 'error';
                message = 'User doesnt exist';
            }
            let data = {'State':state, 'Message':message, 'User': userlogged}
            res.json(data)
        })
    }
}