
import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import { SupplyTypeSchema } from '../models/SupplyTypeModel';

import axios from 'axios';
import * as moment from 'moment/moment';

const SupplyTypes = mongoose.model('SupplyTypes', SupplyTypeSchema);


export class UplanSupplyTypeController{
    public addSupplyType (req: Request, res: Response) {                
        let newSupplyType = new SupplyTypes(req.body);
        newSupplyType.save((err, supplyType) => {
            if(err){
                res.send(err);
            }
            let data = {'State':'success', 'Message':'Supply type add successfully', 'SupplyType': supplyType}
            res.json(data)    
        });
    }
    public SupplyType (req: Request, res: Response) {                
        SupplyTypes.find({}, (err, supplytype) => {
            if(err)
                res.send(err);
            res.json(supplytype);
        });
    }
}