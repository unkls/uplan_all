import {Request, Response} from "express";
import { UplanSupplyController } from "../controllers/SupplyController";
import { UplanUserController } from "../controllers/UserController";
import { UplanSupplyTypeController } from "../controllers/SupplyTypeController";
import { UplanBookingController } from "../controllers/BookingController";

export class Routes {       
    public UplanSupplyController: UplanSupplyController = new UplanSupplyController() 
    public UplanUserController: UplanUserController = new UplanUserController() 
    public UplanSupplyTypeController: UplanSupplyTypeController = new UplanSupplyTypeController() 
    public UplanBookingController: UplanBookingController = new UplanBookingController() 


    public routes(app): void {          
        app.route('/')
        .get((req: Request, res: Response) => {            
            res.status(200).send({
                message: 'Welcome to Uplan API'
            })
        })


        app.route('/user')
        .post(this.UplanUserController.addUser);
        app.route('/connectuser')
        .post(this.UplanUserController.connectUser);
        app.route('/supplytype')
        .post(this.UplanSupplyTypeController.addSupplyType)
        .get(this.UplanSupplyTypeController.SupplyType);
        app.route('/supply')
        .post(this.UplanSupplyController.addSupply)
        .get(this.UplanSupplyController.allSupplies);
        app.route('/supply/:supplyId')
        .get(this.UplanSupplyController.supply);
        app.route('/book')
        .post(this.UplanBookingController.needBook);
        app.route('/acceptbook/:bookId/:action')
        .get(this.UplanBookingController.acceptBook);
        app.route('/userbook/:userId')
        .get(this.UplanBookingController.userBook);
    }
}