import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const BookingSchema = new Schema({
    start: {
        type: Number,
        required: 'Enter a timestamp date',
        default: 0
    }, 
    end: {
        type: Number,
        required: 'Enter a timestamp date',
        default: 0
    }, 
    needAccept: {
        type: Boolean,
        required: 'Need accept or not?',
        default: true
    },  
    Supply: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Supply'
    },
    User: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    }
});