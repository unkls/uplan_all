import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const SupplySchema = new Schema({
    label: {
        type: String,
        required: 'Enter a label'
    }, 
    description : {
        type: String,
        required: 'Enter a description'
    }, 
    SupplyType: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'SupplyType'
    }
});