import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const SupplyTypeSchema = new Schema({
    name: {
        type: String,
        required: 'Enter a string'
    },
    emailRefferer: {
        type: String,
        required: 'Enter an email'
    }
});