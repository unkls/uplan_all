module.exports = {
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'sass',
      patterns: [
        './src/assets/styles/abstracts/variables.styl',
        './src/assets/styles/abstracts/**/*.styl'
      ]
    }
  }

}
