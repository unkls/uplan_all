module.exports = {
  plugins: {
    'postcss-fixes': {
      preset: 'safe'
    },
    'autoprefixer': { /* to edit target browsers: edit ".browserslistrc" ;) */ },
    'css-mqpacker': {
      sort: true
    },
    'cssnano': {
      preset: ['default', {
        zindex: false,
        mergeRules: true,
        reduceTransforms: false,
        autoprefixer: false,
        colormin: false,
        discardComments: {
          removeAll: true
        }
      }]
    },
    'postcss-pxtorem': {
      rootValue: 16,
      unitPrecision: 5,
      propList: [
        'font',
        'font-size',
        'font-size-adjust',
        'line-height',
        'letter-spacing',
        'text-indent',
        'margin',
        'margin-top',
        'margin-right',
        'margin-bottom',
        'margin-left',
        'padding',
        'padding-top',
        'padding-right',
        'padding-bottom',
        'padding-left',
        'width',
        'height',
        'min-width',
        'min-height',
        'max-width',
        'max-height',
        'box-shadow',
        'text-shadow',
        'top',
        'right',
        'bottom',
        'left',
        'border',
        'border-width',
        'border-top-width',
        'border-right-width',
        'border-bottom-width',
        'border-left-width',
        'border-top',
        'border-right',
        'border-bottom',
        'border-left',
        'border-radius',
        'background',
        'background-size',
        'background-position',
        'background-position-x',
        'background-position-y',
        'outline',
        'outline-width',
        'transform'
      ],
      selectorBlackList: [],
      replace: false,
      mediaQuery: false,
      minPixelValue: 0
    },
    'postcss-em-media-query': {}
  }
}
