import { Vue, Component } from 'vue-property-decorator'

import '@/plugins/portal-vue'
import '@/plugins/vee-validate'

import router from '@/router'
import store from '@/store'

import CApp from '@/components/App.vue'

Vue.config.productionTip = false

// Register the custom hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
])

// Global instance properties
Vue.mixin({
  computed: {
    fields () {
      return {}
    },
    formFieldAreValid () {
      let res = false
      for (const key in this.fields) {
        if (this.fields.hasOwnProperty(key)) {
          if (this.fields[key].valid) {
            res = true
          } else {
            return false
          }
        }
      }
      return res
    }
  }
})

new Vue({
  router,
  store,
  render: (h) => h(CApp)
}).$mount('#app')
