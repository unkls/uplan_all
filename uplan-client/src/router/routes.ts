// tslint:disable:max-line-length

// Layouts
import LDefault from '@/layouts/default/Default.vue'
import LCerberus from '@/layouts/cerberus/Cerberus.vue'

// Routes
import VHome from '@/views/Home.vue'
import VSingleItem from '@/views/SingleItem.vue'
import VLogin from '@/views/Login.vue'
import VPasswordForgot from '@/views/PasswordForgot.vue'
import VPasswordReset from '@/views/PasswordReset.vue'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: VHome,
    meta: {
      layout: LDefault,
      order: 0
    }
  },
  {
    path: '/login',
    name: 'login',
    component: VLogin,
    meta: {
      layout: LCerberus,
      order: 0
    }
  },
  {
    path: '/password-forgot',
    name: 'password-forgot',
    component: VPasswordForgot,
    meta: {
      layout: LCerberus,
      order: 1
    }
  },
  {
    path: '/password-reset',
    name: 'password-reset',
    component: VPasswordReset,
    meta: {
      layout: LCerberus,
      order: 1
    }
  },
  {
    path: '/:uuid',
    name: 'single-item',
    component: VSingleItem,
    meta: {
      layout: LDefault,
      order: 1
    }
  }
]
