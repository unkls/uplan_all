import Vue from 'vue'
import Router, { Route } from 'vue-router'

import { routes } from './routes'

Vue.use(Router)

const router: Router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'is-Active',
  linkExactActiveClass: 'is-ExactAndActive'
})

export default router
